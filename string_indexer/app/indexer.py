"""
This is a custom StringIndexer

"""

from pyspark.ml.feature import StringIndexer
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineEstimator


class CustomStringIndexer(StringIndexer, AbstractSparkPipelineEstimator):
    
    def __init__(self, name, xpresso_run_name, \
                        inputCol=None, outputCol=None, \
                        handleInvalid='skip', stringOrderType='frequencyDesc'):

        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol, \
                    handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.name = name
        self.xpresso_run_name = xpresso_run_name

    def _fit(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        model = super()._fit(dataset)
        return model

class LabelIndexer(StringIndexer, AbstractSparkPipelineEstimator):
    
    def __init__(self, name, xpresso_run_name, \
                            inputCol=None, outputCol=None, \
                            handleInvalid='skip', stringOrderType='frequencyDesc'):

        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol, \
                    handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.name = name
        self.xpresso_run_name = xpresso_run_name
    
    def _fit(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        model = super()._fit(dataset)
        return model