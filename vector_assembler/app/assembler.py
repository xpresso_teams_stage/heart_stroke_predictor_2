"""
This is a custom StringIndexer

"""

from pyspark.ml.feature import VectorAssembler
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineTransformer


class CustomVectorAssembler(VectorAssembler, AbstractSparkPipelineTransformer): 

    def __init__(self, name, xpresso_run_name, inputCols=None, outputCol=None):
        class_name = self.__class__.__name__
        VectorAssembler.__init__(self, \
                                inputCols=inputCols, \
                                outputCol=outputCol)
        AbstractSparkPipelineTransformer.__init__(self, name=name)
        self.name = name
        self.xpresso_run_name = xpresso_run_name
    
    def _transform(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        ds = super()._transform(dataset)
        self.state = ds
        return ds