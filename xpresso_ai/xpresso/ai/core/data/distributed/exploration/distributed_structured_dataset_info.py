""" Class definition of xdm Info """

import itertools

import databricks.koalas as ks
import tqdm

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidDatatypeException
from xpresso.ai.core.commons.utils.constants import DEFAULT_PROBABILITY_BINS, \
    DEFAULT_GARBAGE_THRESHOLD
from xpresso.ai.core.data.automl.dataset_type import DatasetType, DECIMAL_PRECISION
from xpresso.ai.core.data.exploration.attribute_info import AttributeInfo
from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ['StructuredDatasetInfo']
__author__ = 'Sanyog Vyawahare'

# This is indented as logger can not be serialized and can not be part
# of xdm
logger = XprLogger()


class StructuredDatasetInfo:
    """ DatasetInfo contains the detailed information about the
    xdm. This information contains the attribute list,
    attribute type, attribute metrics"""

    def __init__(self):
        self.attributeInfo = list()
        self.metrics = dict()
        return

    def understand_attributes(self, data,
                              dataset_type: DatasetType.DIST_STRUCTURED):
        """
        Understands the data and finds the type of all the attributes
        :return:
        """
        if not isinstance(dataset_type, DatasetType) or dataset_type is \
                DatasetType.UIMAGE or dataset_type is DatasetType.UTEXT:
            logger.error("Unacceptable Data type provided. Type {} is "
                         "not supported".format(dataset_type))
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(dataset_type))

        self.attributeInfo = list(map(lambda x: AttributeInfo(x, dataset_type),
                                      data.columns))
        print("\nStarting Data Understanding:\n")
        for attr in tqdm.tqdm(self.attributeInfo):
            attr.dtype = data[attr.name].dtype
            attr.type, attr.dtype = self.find_attr_type(data[attr.name],
                                                        attr.dtype)
            if attr.type is DataType.DATE.value:
                data[attr.name] = ks.to_datetime(data[attr.name], errors="coerce")
                attr.dtype = data[attr.name].dtype

    @staticmethod
    def is_date(date_values):
        """
        Parse the date value to check if it's valid
        :return: Bool result
        """
        is_na_count = ks.to_datetime(date_values, errors="coerce").isna().sum()
        is_date_count = date_values.size - is_na_count
        percent_threshold = 0.95
        ret = (is_date_count > date_values.size * percent_threshold)
        return ret

    @staticmethod
    def parse_date(date_values):
        """
        It parses date_value to date format if possible, else returns the same
        value
        Args:
            date_values(:str): Series of    data to be parsed into date
        Returns:
            Parsed value of date_value to date if possible
        """
        ret = ks.to_datetime(date_values, errors="coerce")
        return ret

    # Below method finds the data type of the attributes
    @staticmethod
    def find_attr_type(data, dtype, threshold=5, length_threshold=50):
        """
        Find the attribute data type
        :return: data type and type of attribute
        """
        num_rows = float(data.size)
        unique_count = data.nunique(approx=True)
        unique_proportion = (float(unique_count) / num_rows) * 100
        dtype = str(dtype)

        data_type = DataType.STRING.value
        if DataType.FLOAT.value in dtype or DataType.INT.value in dtype:
            if unique_proportion < threshold:
                data_type = DataType.NOMINAL.value
            else:
                data_type = DataType.NUMERIC.value

        elif DataType.PD_DATETIME.value in dtype:
            data_type = DataType.DATE.value

        elif DataType.OBJECT.value in dtype:
            max_length = data.loc[~data.isna()].map(
                lambda x: len(x) if type(x) == str else 0).max()
            temp_data = data.astype(DataType.FLOAT.value)
            na_count = temp_data.isna().sum()
            na_percent = na_count / num_rows * 100

            if na_percent < DEFAULT_GARBAGE_THRESHOLD:
                dtype = temp_data.dtype

                if unique_proportion < threshold:
                    data_type = DataType.NOMINAL.value
                else:
                    data_type = DataType.NUMERIC.value

            elif StructuredDatasetInfo.is_date(data):
                data_type = DataType.DATE.value

            elif unique_proportion < threshold:
                data_type = DataType.NOMINAL.value

            elif max_length < length_threshold:
                data_type = DataType.STRING.value

            else:
                data_type = DataType.TEXT.value

        elif DataType.BOOL.value in dtype:
            data_type = DataType.NOMINAL.value

        return data_type, dtype

    def populate_attribute(self, data, date_type, threshold,
                           bins=DEFAULT_PROBABILITY_BINS):
        """
         Populate the metrics for all the attributes
        """
        if not isinstance(date_type, DatasetType) or date_type is not \
                DatasetType.DIST_STRUCTURED:
            logger.error("Unacceptable Data type provided. Type {} is "
                         "not supported".format(date_type))
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(date_type))

        # For structured datatype
        print("\nStarting UniVariate Exploration:\n")
        for attr in tqdm.tqdm(self.attributeInfo):
            attr.populate(data[attr.name], threshold, bins=bins)

    def populate_metric(self, data, data_type):
        """
        Populates metric for all data types
        :param data: Input data
        :param data_type: Data type of the input data
        """

        if not isinstance(data_type, DatasetType) or data_type is not \
                DatasetType.STRUCTURED:
            logger.error("Unacceptable Data type provided. Type {} is "
                         "not supported".format(data_type))
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(data_type))

        # For structured datatype
        print("Starting Multivariate Exploration:\n")
        data_copy = data
        self.metrics["num_records"] = len(data_copy)
        self.metrics["num_attributes"] = len(data_copy.columns)

        na_count, na_count_percentage, missing_count, \
        missing_count_percentage = self.na_analysis(
            data_copy)

        self.metrics["na_count"] = na_count
        self.metrics["na_count_percentage"] = na_count_percentage
        self.metrics["missing_count"] = missing_count
        self.metrics["missing_count_percentage"] = missing_count_percentage

        duplicates_count, duplicates_count_percentage, \
        duplicate_rows_count = self.duplicates_analysis(data_copy)

        self.metrics["duplicate_count"] = duplicates_count
        self.metrics["duplicate_count_percentage"] = duplicates_count_percentage
        self.metrics["duplicate_rows_count"] = duplicate_rows_count

        numeric_field = list()
        nominal_field = list()
        ordinal_field = list()
        date_field = list()
        string_field = list()
        text_field = list()
        for val in self.attributeInfo:
            if val.type is DataType.NUMERIC.value:
                numeric_field.append(val.name)
            elif val.type is DataType.NOMINAL.value:
                nominal_field.append(val.name)
            elif val.type is DataType.ORDINAL.value:
                ordinal_field.append(val.name)
            elif val.type is DataType.DATE.value:
                date_field.append(val.name)
            elif val.type is DataType.STRING.value:
                string_field.append(val.name)
            elif val.type is DataType.TEXT.value:
                text_field.append(val.name)
        self.metrics["numeric_attributes"] = numeric_field
        self.metrics["num_numeric"] = len(numeric_field)
        self.metrics["num_nominal"] = len(nominal_field)
        self.metrics["num_ordinal"] = len(ordinal_field)
        self.metrics["num_date"] = len(date_field)
        self.metrics["num_string"] = len(string_field)
        self.metrics["num_text"] = len(text_field)

        if len(numeric_field) != 0:
            self.metrics["pearson"] = data[numeric_field].dropna().corr(
                method="pearson").round(DECIMAL_PRECISION).unstack().to_dict()

        if len(numeric_field + ordinal_field) != 0:
            self.metrics["spearman"] = data[numeric_field + ordinal_field].dropna().corr(
                method="spearman").round(DECIMAL_PRECISION).unstack().to_dict()

        nominal_nominal_comb = list(
            itertools.product(nominal_field, nominal_field))
        nominal_ordinal_comb = list(itertools.product(nominal_field,
                                                      ordinal_field))
        chisquare_combination = nominal_nominal_comb + nominal_ordinal_comb

        if len(chisquare_combination) > 0:
            self.metrics["chi_square"] = dict()

        for val in tqdm.tqdm(chisquare_combination):
            self.metrics["chi_square"][val] = self.ChiSquareTest(data,
                                                                 val[0],
                                                                 val[1])
            try:
                self.metrics["chi_square"][val] = self.ChiSquareTest(data,
                                                                     val[0],
                                                                     val[1])
            except IndexError:
                pass

    @staticmethod
    def duplicates_analysis(df):
        """Identifying count of duplicate rows in the data
        Args:
            df(:obj:`DataFrame`): data in which count of duplicate rows is to be
                                    added
        Returns:
            list_tuples(list): list of tuples containing a row and it's
                                corresponding duplicate count
        """
        # a new column called "duplicate_rows_count" is created
        temp_data = ks.DataFrame(
            df.groupby(df.columns.to_list(), as_index=False).size())
        data = temp_data.rename(
            columns={0: "count"}).reset_index()

        # a list of duplicate rows count
        list_dup_row_count = data.to_dict(orient="list").get('count')

        # dropping the newly made column
        non_dup_data = data.drop(columns="count")

        # creating a list of tuples containing a row and it's corresponding
        # duplicate count
        list_tuples = []
        for i in range(non_dup_data.shape[0]):
            tup = (list(non_dup_data.loc[[i], non_dup_data.columns]),
                   list_dup_row_count[i] - 1)
            list_tuples.append(tup)

        num_rows = float(df.size)
        duplicates_count = sum(df.duplicated().to_numpy())
        duplicates_count_percentage = round(float(duplicates_count / num_rows) *
                                            100, DECIMAL_PRECISION)

        return duplicates_count, duplicates_count_percentage, list_tuples

    @staticmethod
    def na_analysis(data):
        """Calculates the NA fields and missing fields in the data
        Args:
            data (:obj:`Dataframe`): the data in which number of
                missing fields and null fields are to be calculated
        Return:
            na_count (:int): Number of NA fields
            na_count_percentage (:float): Percentage of NA fields
            missing_count (:int): Number of missing data
            missing_count_percentage (:float): Percentage of missing data
        """
        num_rows = float(data.size)
        # TODO test ks.Dataframe.apply()
        # na_count = sum(data.isnull().apply(lambda x: all(x), axis=1))
        # na_count_percentage = round(float(na_count / num_rows) * 100,
        #                                DECIMAL_PRECISION)
        na_count = 0
        na_count_percentage = 0
        missing_count = int((data == "").sum().sum())
        missing_count_percentage = round(float(missing_count / num_rows) * 100,
                                         DECIMAL_PRECISION)

        return na_count, na_count_percentage, missing_count, \
               missing_count_percentage

    @staticmethod
    def ChiSquareTest(df, x, y):
        """Calculates the chi square correlation between two attributes
        Args:
             x(:list): List of values of one attribute
             y(:list): List of values of another attribute
        Returns:
            (int): Chi-square correlation coefficient
         """
        # TODO test ks.crosstab()

        # x = df[x].astype(str)
        # y = df[y].astype(str)

        # dfObserved = pd.crosstab(y, x)
        # chi2, p_value, dof, expected = stats.chi2_contingency(dfObserved.values)
        # return round(p_value, DECIMAL_PRECISION)
        return 0
