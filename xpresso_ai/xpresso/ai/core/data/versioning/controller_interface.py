import abc
# from xpresso.ai.core.data.versioning.versioning_authenticator import VersioningAuthenticator
# auth = VersioningAuthenticator()


class VersionControllerInterface(metaclass=abc.ABCMeta):
    """"""
    def __init__(self, **kwargs):
        # __init__ of inherited controller class should make a call to
        # authenticate_session method of VersioningAuthenticator
        pass

    # @auth.authenticate_request
    @abc.abstractmethod
    def create_repo(self, *args, **kwargs):
        """"""

    # @auth.filter_repo
    @abc.abstractmethod
    def list_repo(self):
        """"""
        print("this getting printed")

    # @auth.authenticate_request
    @abc.abstractmethod
    def create_branch(self, *args, **kwargs):
        """"""

    @abc.abstractmethod
    def list_branch(self, *args):
        """"""

    # @auth.authenticate_request
    @abc.abstractmethod
    def push_dataset(self, *args, **kwargs):
        """"""

    # @auth.authenticate_request
    @abc.abstractmethod
    def pull_dataset(self, *args, **kwargs):
        """"""

    # @auth.authenticate_request
    @abc.abstractmethod
    def list_dataset(self, *args, **kwargs):
        """"""
