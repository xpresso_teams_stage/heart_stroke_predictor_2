from abc import ABC
from os import path as path_library

from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.data.versioning.pachyderm.client \
    import PachydermClient
import xpresso.ai.core.data.automl.dataset as dataset_module
from xpresso.ai.core.data.versioning.utils \
    import name_validity_check, fetch_file_path_list
from xpresso.ai.core.data.versioning.resource_management.repo_manager \
    import RepoManager
from xpresso.ai.core.data.versioning.resource_management.branch_manager \
    import BranchManager
from xpresso.ai.core.data.versioning.resource_management.file_manager \
    import DatasetManager
from xpresso.ai.core.data.automl import StructuredDataset, UnstructuredDataset
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.commons.exceptions.xpr_exceptions import *
from xpresso.ai.core.data.versioning.controller_interface \
    import VersionControllerInterface
from xpresso.ai.core.data.versioning.versioning_authenticator \
    import VersioningAuthenticator

auth = VersioningAuthenticator()


class PachydermVersionController(VersionControllerInterface):
    """
    Manages repos on pachyderm cluster
    """
    # user input variables
    INPUT_BRANCH_NAME = "branch_name"
    INPUT_COMMIT_ID = "commit_id"
    INPUT_DATASET_NAME = "dataset_name"
    INPUT_DESCRIPTION = "description"
    INPUT_REPO_NAME = "repo_name"
    INPUT_PATH = "path"
    INPUT_DATASET = "dataset"
    LIST = "list"
    PULL = "pull"
    DATA_TYPE = "data_type"
    DATA_TYPE_IS_FILE = "files"
    # Internal Code specific variables
    ACCEPTED_FIELD_NAME_REGEX = r"[\w, -]+$"
    DEFAULT_LIST_DATASET_PATH = "/"
    PICKLE_FILE_EXTENSION = ".pkl"
    PUSH_FILES_MANDATORY_FIELDS = ["repo_name", "branch_name", "data_type",
                                   "dataset_name", "path", "description"]
    PUSH_DATASET_MANDATORY_FIELDS = ["repo_name", "branch_name", "dataset",
                                     "description"]
    COMMIT_INFO_ID = "commit_id"
    COMMIT_INFO_BRANCH = "branch_name"
    # output variables
    REPO_OUTPUT_NAME = "repo_name"
    FILE_TYPE = "type"
    OUTPUT_COMMIT_FIELD = "commit"
    OUTPUT_COMMIT_ID = "id"
    OUTPUT_BRANCH_HEAD = "head"
    PULL_OUTPUT_TYPE = "output_type"
    OUTPUT_FILE_TYPE = "files"
    PROJECTS = "projects"
    SUPER_USER = "su"

    def __init__(self, pachyderm_controller_info, **kwargs):
        """
        constructor class for PachydermVersionController
        """
        super().__init__(**kwargs)
        self.logger = XprLogger()
        try:
            self.pachyderm_client = PachydermClient(
                pachyderm_controller_info["host"],
                pachyderm_controller_info["port"]
            )
            self.repo_manager = RepoManager(self.pachyderm_client)
            self.branch_manager = BranchManager(self.pachyderm_client)
            self.dataset_manager = DatasetManager(self.pachyderm_client)
            self.commit_manager = self.dataset_manager.commit_manager
            auth.kwargs = kwargs
            auth.authenticate_session()
        except PachydermOperationException as err:
            raise ValueError(err.message)

    @auth.authenticate_request
    def create_repo(self, **kwargs):
        """
        creates a new repo on pachyderm cluster

        Args:
            kwargs(keyword arguments):
                repo_name(str): name of the repo to be created
                description(str): brief description of this repo
        """
        if self.INPUT_REPO_NAME not in kwargs:
            raise RepoNotProvidedException()
        description = ""
        if self.INPUT_DESCRIPTION in kwargs:
            description = kwargs[self.INPUT_DESCRIPTION]
        self.repo_manager.create(kwargs[self.INPUT_REPO_NAME], description)

    @auth.filter_repo
    def list_repo(self):
        """
        returns the list of all available repos

        :return:
            returns list of repos
        """
        repo_info = self.repo_manager.list()
        if not len(repo_info):
            raise RepoPermissionException("No repos found")
        return repo_info

    @auth.authenticate_request
    def create_branch(self, **kwargs):
        """
        creates a new branch in specified repo on pachyderm cluster

        Args:
            kwargs:
                info of the branch to be created
                keys - repo_name, branch_name
                repo_name: name of the repo
                branch_name: name of the branch
        :return:
        """
        if self.INPUT_REPO_NAME not in kwargs or \
                self.INPUT_BRANCH_NAME not in kwargs:
            raise BranchInfoException("Repo name & branch name is required")

        repo_name = kwargs[self.INPUT_REPO_NAME]
        try:
            self.repo_manager.validate_repo(repo_name)
        except RepoInfoException as err:
            # lazy creation of repo
            self.logger.debug("Lazy creating a new repo in create_branch")
            self.repo_manager.create(repo_name)

        self.branch_manager.create(
            repo_name,
            kwargs[self.INPUT_BRANCH_NAME]
        )

    @auth.authenticate_request
    def list_branch(self, repo_name):
        """
        returns a list of branches in specified repo

        :param repo_name:
            name of the repo to list the branches
        """
        self.repo_manager.validate_repo(repo_name)

        branch_info = self.branch_manager.list(repo_name)
        return branch_info

    @auth.authenticate_request
    def push_dataset(self, **kwargs):
        """
        pushes file/files into a pachyderm cluster

        :param kwargs:
        :keyword Arguments:
            *repo_name:
                name of the repo
            *branch_name:
                name of the branch
            *dataset:
                AbstractDataset object with info on dataset
            *dataset_name:
                name of dataset that will be pushed to cluster
            *path:
                local path of file or list of files i.e a directory
            *description:
                A brief description on this push
        """
        # data_type field is manually added by controller
        # client before sending request to push_dataset
        self.validate_push_dataset_input(**kwargs)
        if self.DATA_TYPE in kwargs and \
                kwargs[self.DATA_TYPE] == self.DATA_TYPE_IS_FILE:
            # when data_type is files push all files directly to cluster
            path_to_data = kwargs[self.INPUT_PATH]
            dataset_name = kwargs[self.INPUT_DATASET_NAME]
        else:
            # when data_type is not files, then it is assumed to be a dataset
            dataset_name = kwargs[self.INPUT_DATASET].name
            path_to_data = kwargs[self.INPUT_DATASET].save()

        if not path_library.exists(path_to_data):
            raise DatasetPathException(f"path {path_to_data} is invalid")
        elif path_library.isfile(path_to_data):
            dataset_dir = path_library.dirname(path_library.abspath(path_to_data))
        else:
            dataset_dir = path_to_data
        # check is the dataset input is valid or not
        self.verify_dataset_input(**kwargs)
        # fetches the path of all the files inside the dataset directory
        file_list = fetch_file_path_list(dataset_dir, dataset_name)
        new_commit_id = self.pachyderm_client.push_dataset(
            kwargs[self.INPUT_REPO_NAME],
            kwargs[self.INPUT_BRANCH_NAME],
            file_list,
            kwargs[self.INPUT_DESCRIPTION]
        )

        return new_commit_id, f"/dataset/{dataset_name}"

    @auth.authenticate_request
    def pull_dataset(self, **kwargs):
        """
        pulls a dataset from pachyderm cluster and load it locally

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns the path of the directory where dataset is saved
        """
        list_output = self.list_dataset(**kwargs)
        output = self.dataset_manager.pull(kwargs[self.INPUT_REPO_NAME], list_output)
        if self.PULL_OUTPUT_TYPE in kwargs and \
                kwargs[self.PULL_OUTPUT_TYPE] == self.OUTPUT_FILE_TYPE:
            return output
        # TODO: Add abstract resource
        new_dataset = dataset_module.AbstractDataset()
        new_dataset.load(output)
        # Get dataset object of specific type
        new_dataset = self.get_dataset_object(new_dataset)
        new_dataset.load(output)
        return new_dataset

    @auth.authenticate_request
    def list_dataset(self, **kwargs):
        """
        list of dataset as per provided information

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns a dict with file and commit info
        """
        # verifies provided info and returns a dict with request input fields
        self.verify_dataset_input(**kwargs)
        data_info = self.complete_dataset_info(**kwargs)
        if data_info[self.INPUT_COMMIT_ID] == '-':
            raise DatasetInfoException("No commits were made to this branch"
                                       " to fetch any dataset")
        list_output = self.dataset_manager.list(
            data_info[self.INPUT_REPO_NAME],
            data_info[self.INPUT_COMMIT_ID],
            data_info[self.INPUT_PATH]
        )
        return list_output

    def verify_dataset_input(self, **kwargs):
        """
        verifies input parameters for dataset operations i.e. push, pull & list

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
        """
        if self.INPUT_REPO_NAME not in kwargs:
            raise RepoNotProvidedException
        self.repo_manager.validate_repo(kwargs[self.INPUT_REPO_NAME])

        if self.INPUT_COMMIT_ID not in kwargs and \
                self.INPUT_BRANCH_NAME not in kwargs:
            raise DatasetInfoException(
                "Either one of commit_id or branch_name is mandatory"
            )
        if self.INPUT_BRANCH_NAME in kwargs:
            self.branch_manager.check_branch_existence(
                kwargs[self.INPUT_REPO_NAME],
                kwargs[self.INPUT_BRANCH_NAME]
            )

    def complete_dataset_info(self, **kwargs):
        """
        verifies if the provided info for a dataset is valid or not

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns updated info else throws an exception
            in case of error
        """
        data_info = {}
        data_info[self.INPUT_REPO_NAME] = kwargs[self.INPUT_REPO_NAME]
        if self.INPUT_COMMIT_ID in kwargs:
            commit_info = self.commit_manager.inspect(
                kwargs[self.INPUT_REPO_NAME],
                kwargs[self.INPUT_COMMIT_ID]
            )
            if self.INPUT_BRANCH_NAME in kwargs and \
                    commit_info[self.COMMIT_INFO_BRANCH] != kwargs[self.INPUT_BRANCH_NAME]:
                raise DatasetInfoException(
                    "commit_id and branch_name info conflicts"
                )

            data_info[self.INPUT_COMMIT_ID] = commit_info[self.COMMIT_INFO_ID]
            data_info[self.INPUT_BRANCH_NAME] = commit_info[self.COMMIT_INFO_BRANCH]
        else:
            branch_info = self.branch_manager.inspect(
                kwargs[self.INPUT_REPO_NAME],
                kwargs[self.INPUT_BRANCH_NAME]
            )
            data_info[self.INPUT_BRANCH_NAME] = kwargs[self.INPUT_BRANCH_NAME]
            data_info[self.INPUT_COMMIT_ID] = branch_info[self.OUTPUT_BRANCH_HEAD]

        data_info[self.INPUT_PATH] = self.DEFAULT_LIST_DATASET_PATH
        if self.INPUT_PATH in kwargs:
            data_info[self.INPUT_PATH] = kwargs[self.INPUT_PATH]
        return data_info

    @auth.authenticate_request
    def list_commit(self, repo_name, branch_name):
        """
        lists all the commits in a repo under a branch

        :param repo_name:
            name of the repo
        :param branch_name:
            name of the branch
        :return:
            list of commits
        """
        self.repo_manager.validate_repo(repo_name)
        return self.commit_manager.list(repo_name, branch_name)

    def validate_push_dataset_input(self, **kwargs):
        """

        :param kwargs:
        :return:
        """
        if self.DATA_TYPE in kwargs and \
                kwargs[self.DATA_TYPE] == self.DATA_TYPE_IS_FILE:
            mandatory_fields = self.PUSH_FILES_MANDATORY_FIELDS
            exempt_fields = [self.INPUT_PATH]
            # path can have '/'. Hence excluded from this check
        else:
            mandatory_fields = self.PUSH_DATASET_MANDATORY_FIELDS
            exempt_fields = self.INPUT_DATASET
            abstract_dataset = dataset_module.AbstractDataset
            if not isinstance(kwargs[self.INPUT_DATASET], abstract_dataset):
                raise DatasetInfoException("Provided dataset is invalid")

        for field in mandatory_fields:
            if field not in kwargs:
                raise DatasetInfoException(f"'{field}' field not provided")
            elif field not in exempt_fields:
                name_validity_check(field, kwargs[field])
        for field in kwargs:
            if field not in mandatory_fields:
                raise DatasetInfoException(f"invalid field '{field}' provided")
        # checks if the repo is valid and does exists
        self.repo_manager.validate_repo(kwargs[self.INPUT_REPO_NAME])

    def get_dataset_object(self, dataset):
        """
        Return the specific type of dataset depending upon the type
        attribute

        :param dataset:
            Abstract dataset object
        :return:
            Specific datset object
        """
        try:
            if dataset.type == DatasetType.STRUCTURED:
                return StructuredDataset()
            elif dataset.type == DatasetType.UTEXT:
                return UnstructuredDataset()
            elif dataset.type == DatasetType.DIST_STRUCTURED:
                print("Unsupported for distributed dataset. Try HDFS "
                      "version controller")
                raise DatasetInfoException("Unsupported for distributed dataset. Try HDFS "
                                           "version controller")
        except AttributeError:
            self.logger.error("Dataset type attribute not present")
            raise DatasetInfoException("Dataset type attribute not present")
