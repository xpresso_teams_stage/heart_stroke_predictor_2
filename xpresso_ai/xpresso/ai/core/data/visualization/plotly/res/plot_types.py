# Developed By Nalin Ahuja, nalin.ahuja@abzooba.com

import plotly.graph_objs as go
from xpresso.ai.core.data.visualization.plotly.res.plot_struct import std_format

class cartesian(std_format):
    def apply_default_style(self):
        super(cartesian, self).apply_default_style()
        self.x_min = None
        self.x_max = None
        self.y_min = None
        self.y_max = None

    def set_x_range(self, min_x=None, max_x=None):
        if (not (min_x is None)):
            self.x_min = min_x
        if (not (max_x is None)):
            self.x_max = max_x

    def set_y_range(self, min_y=None, max_y=None):
        if (not (min_y is None)):
            self.y_min = min_y
        if (not (max_y is None)):
            self.y_max = max_y

    def check_ranges(self):
        # Error Check Plot Xaxis Range
        if (self.x_axis_title is None):
            self.x_axis_title = go.layout.XAxis(range=[self.x_min, self.x_max])
        else:
            self.x_axis_title['range'] = [self.x_min, self.x_max]

        # Error Check Plot Yaxis Range
        if (self.y_axis_title is None):
            self.y_axis_title = go.layout.YAxis(range=[self.y_min, self.y_max])
        else:
            self.y_axis_title['range'] = [self.y_min, self.y_max]


class diagram(std_format):
    def apply_default_style(self):
        super(diagram, self).apply_default_style()
