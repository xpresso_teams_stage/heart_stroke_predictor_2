""" Parses configuration file to create an internal dictionary file"""

__all__ = ['XprConfigParser']
__author__ = 'Naveen Sinha'

import os
import json
import jsonmerge
from json import JSONDecodeError

from xpresso.ai.core.commons.utils.generic_utils import get_base_pkg_location, \
    get_default_config_path
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidConfigException


class XprConfigParser:
    """ Parses properties file and stores them in the internal dictionary file.
    It expects the location of config file as an input. By Default, it searches
    config in config/common.json.
    """

    # This is used when no configuration is provided
    DEFAULT_CONFIG_PATH = get_default_config_path()
    BASE_CONFIG_PATH = "config/common.json"
    config_file_path = ""

    def __init__(self, config_file_path=DEFAULT_CONFIG_PATH, use_base=True):

        self.config_json = None
        if use_base:
            self.config_file_path = self.get_default_config_path()
            self.populate_config(config_path=self.config_file_path)
        if config_file_path != self.BASE_CONFIG_PATH:
            self.config_file_path = os.path.join(get_base_pkg_location(),
                                                 config_file_path)
            self.populate_config(config_path=self.config_file_path)

    def get_default_config_path(self):
        """ Returns the absolute path of default configuration file """
        return os.path.join(get_base_pkg_location(), self.BASE_CONFIG_PATH)

    def populate_config(self, config_path: str):
        """ Loads configuration and merge it with the previous configuration
         Args:
             config_path(str): path to the configuration file
        """
        if not self.config_json:
            self.config_json = {}
        self.config_json = jsonmerge.merge(
            self.config_json,
            self.parse_json(config_path=config_path))

    @staticmethod
    def parse_json(config_path: str):
        """ Parses the JSON configuration
        Args:
             config_path(str): path to the configuration file

        Returns:
            Json Object: parsed json object
        Throws:
            InvalidConfigException: raised if there is an issue with the json
                                    file
        """
        try:
            json_fs = open(config_path, 'r', encoding='utf-8')
            json_object = json.load(json_fs)
            return json_object
        except FileNotFoundError as e:
            raise InvalidConfigException("{} config file path is invalid. {}"
                                         .format(config_path, str(e)))
        except (JSONDecodeError, TypeError) as e:
            raise InvalidConfigException("{} is invalid json in config. {}"
                                         .format(config_path, str(e)))

    def __getitem__(self, key):
        """
        Overriding get method to support direct return from self.config_json
        """
        return self.config_json[key]
