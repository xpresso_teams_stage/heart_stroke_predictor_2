#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


## By xpresso.ai - DO NOT CHANGE

if [[ "$SPARK_MASTER_TYPE" == "k8s" ]]
then

export JAVA_HOME=/usr/jdk64/jdk1.8.0_112
export PATH=$SPARK_HOME/bin:$PATH

spark-submit --master "${MASTER}" \
			--deploy-mode cluster \
			--name ${JOB_NAME} \
			--driver-cores 1 \
			--executor-cores ${EXECUTOR_CORES}  \
			--driver-memory ${DRIVER_MEMORY} \
			--executor-memory ${EXECUTOR_MEMORY} \
			--conf spark.kubernetes.namespace=${NAMESPACE} \
			--conf spark.kubernetes.driver.pod.name=${JOB_NAME} \
			--conf spark.executor.instances=${NUM_EXECUTORS} \
			--conf spark.kubernetes.container.image=${APP_IMAGE} \
			--conf spark.kubernetes.container.image.pullPolicy=Always \
			--conf spark.kubernetes.container.image.pullSecrets=${K8S_SPARK_SECRET} \
			--conf spark.kubernetes.authenticate.driver.serviceAccountName=${K8S_SPARK_SA} \
			--conf spark.kubernetes.pyspark.pythonVersion=3 \
			local:///app/app/main.py ${CMD_ARGS}

echo 'Now exiting....'
exit 0
fi


if [[ "$SPARK_MASTER_TYPE" == "yarn" ]]
then
	echo "Generating dependency packages"
	cd ${ROOT_FOLDER}/..
	zip -qr packs.zip ./*
	mv packs.zip ${ROOT_FOLDER}/packs.zip
	cd ${ROOT_FOLDER}
	echo "Packages generated"

	export PYSPARK_PYTHON=python3
	export PYSPARK_DRIVER_PYTHON=python3
	export JAVA_HOME=/usr/jdk64/jdk1.8.0_112
	export HADOOP_CONF_DIR=/etc/hadoop/conf
	export CLASSPATH=/usr/hdp/3.1.4.0-315/spark2/jars
	export SPARK_HOME=/usr/hdp/3.1.4.0-315/spark2/

	spark-submit --master ${MASTER} \
	       --py-files packs.zip \
	       --name ${JOB_NAME} \
	       --conf "JOB_NAME=${JOB_NAME}" \
	       --conf spark.yarn.appMasterEnv.XPRESSO_CONFIG_PATH="config/common_stage.json" \
	       --deploy-mode cluster \
	       --executor-memory ${EXECUTOR_MEMORY} \
	       --executor-cores ${EXECUTOR_CORES}  \
	       --num-executors ${NUM_EXECUTORS} \
	       --driver-memory ${DRIVER_MEMORY} \
	       app/main.py ${CMD_ARGS}

	exit 0
fi
